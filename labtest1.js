const bodyParser = require('body-parser');
const fs = require("fs");
var express = require('express');
var app = express();
const multer = require('multer');
var urlencoder = bodyParser.urlencoded({extended: false})

app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: false}))
app.use(multer({dest: 'public/tmp/'}).single('file'))

app.get('/index.html', (req, res) => {
    console.log('Got a GET request for index file');
    res.sendFile(__dirname+"/index.html")
})

app.post('/file_upload', (req, res) => {
    console.log(req.file.filename);
    console.log(req.file.path);
    console.log(req.file.mimetype);
    var file = __dirname + "/" + req.file.name;

    fs.readFile(req.file.path, (err, data) => {
        fs.writeFile(file, data, (err) => {
            if(err){
                console.log(err);    
            } else {
                response = {
                    message: 'File Uploaded successfully',
                    filename: req.file.filename
                }
            }

            console.log(response);
            res.end(JSON.stringify(response))
        })
    })
})

app.post('/process_post', urlencoder, (req, res) => {
    response = {
        first_name: req.body.first_name,
        last_name: req.body.last_name
    };
    console.log(response)
    res.end(JSON.stringify(response));
})

app.get('/process_get', (req, res) => {
    response = {
        first_name: req.query.first_name,
        last_name: req.query.last_name
    };
    console.log(response)
    res.end(JSON.stringify(response));
})

app.get('/', (req, res) => {
    console.log('Got a GET request for homepage');
    res.send('Hello GET');
})

app.get('/help', (req, res) => {
    console.log('Got a GET request for /help');
    res.send('Hello Help');
})

app.post('/help', (req, res) => {
    console.log('Got a POST request for /help');
    res.send('Sending Post request...');    
})

app.delete('/del_user', (req, res) => {
    console.log('Got a DELETE request for /del_user');
    res.send('Sending Delete request...');
})

app.get('/list_user', (req, res) => {
    console.log('Got a GET request for /list_user');
    res.send('Page Listing');
})

app.get('/ab*cd', (req, res) => {
    console.log('Got a GET request for /ab*cd');
    res.send('Page pattern match');
})

var server = app.listen(8081, () => {
    var host = server.address().address
    var port = server.address().port

    console.log("App listening at http:%s:%s", host, port)
})